const BookStatus = require('./bookStatus')

class Book {
    #name;
    #author;
    status;

    constructor(name, author) {
        this.#name = name;
        this.#author = author;
        this.status = BookStatus.available;
    }

    get name() {
        return this.#name;
    }
}

module.exports = Book