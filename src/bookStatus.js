class BookStatus {
    static available = 'AVAILABLE'
    static checkedOut = 'CHECK_OUT'
}

module.exports = BookStatus