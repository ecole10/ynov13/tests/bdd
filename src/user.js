const BookStatus = require('./bookStatus')

class User {
    static checkoutBook(book) {
        book.status = BookStatus.checkedOut
    }
}

module.exports = User