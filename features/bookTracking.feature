Feature: Book tracking

    Scenario: Check out a book
    Given A user wishes to check out a book from a library with these books
    | Name                  | Author        |
    | Romeo & Juliet        | Shakespeare   |
    | Clean Architecture    | Robert Martin |
    When They check out the book "Clean Architecture"
    Then It should be marked as checked out