const {Given, When, Then} = require('@cucumber/cucumber')
const Should = require('should')

const User = require('../../src/user')
const Book = require('../../src/book')
const BookStatus = require('../../src/bookStatus')

const booksAvailable = [];
let book;

Given('A user wishes to check out a book from a library with these books', function (books) {
    books.hashes().forEach(book => {
        booksAvailable.push(new Book(book.Name, Book.Author))
    })
});

When('They check out the book {string}', function (bookName) {
    book = booksAvailable.find(bookAvailable => bookAvailable.name === bookName)

    if (book) {
        User.checkoutBook(book)
    }
});

Then('It should be marked as checked out', function () {
    Should(book.status).be.equals(BookStatus.checkedOut)
});
